import os
import json

from flask import Flask, current_app, send_file, request, Response, jsonify
from flask_cors import CORS

from .api import api_bp
from .client import client_bp

app = Flask(__name__, static_folder='../dist/static')
app.register_blueprint(api_bp)
# app.register_blueprint(client_bp)
CORS(app)
from .config import Config
app.logger.info('>>> {}'.format(Config.FLASK_ENV))

dir_path = os.path.dirname(os.path.realpath(__file__))
DATABASE = dir_path + '/db.json'
def get_db():
    with open(DATABASE) as f:
        db = json.load(f)
    return db

def save_db(db):
    with open(DATABASE, 'w') as f:
        print(db)
        json.dump(db, f)

@app.route('/')
def index_client():
    dist_dir = current_app.config['DIST_DIR']
    entry = os.path.join(dist_dir, 'index.html')
    return send_file(entry)

@app.route('/books',methods=['GET'])
def index_books():
	return Response(response=json.dumps(get_db()),
                    status=200,
                    mimetype="application/json")

@app.route('/books/<title>', methods=['PUT'])
def update_book(title):
    print(request.json)
    if not request.json or not type(request.json['title']) == str or not type(request.json['author']) == str or not type(request.json['read']) == int:
        return '', 400
    db = get_db()
    for idx, b in enumerate(db):
        if b['title'] == title:
            db[idx] = {'title':request.json['title'], 'author':request.json['author'],'read':request.json['read']}
            print(db)
            save_db(db)
            return '', 200
    return '', 400

@app.route('/books/<title>', methods=['DELETE'])
def delete_book(title):
    db = get_db()
    for index,d in enumerate(db):
        if db[index]["title"] == title:
            tepm_db = [i for i in db if not i["title"] == title]
            save_db(tepm_db)
            return 'deleted successfully' + title, 200
    return 'no book matches provided title ', 400 
    
    
@app.route('/books', methods=['POST'])
def create():
    try:
        book = request.json
        books = get_db()
        books.append(book)
        print(books)
        save_db(books)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}         
    except Exception:
        return json.dumps({'success':False}), 400, {'ContentType':'application/json'} 

@api_bp.after_request
def add_header(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response
