import axios from 'axios'

let $axios = axios.create({
  baseURL: '/api/',
  timeout: 5000,
  headers: { 'Content-Type': 'application/json' }
})

// Request Interceptor
$axios.interceptors.request.use(function (config) {
  config.headers['Authorization'] = 'Fake Token'
  return config
})

// Response Interceptor to handle and log errors
$axios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  // Handle Error
  console.log(error)
  return Promise.reject(error)
})

export default {

  get () {
    return $axios.get(`/books`)
      .then(response => response.data)
  },

  post () {
    return $axios.post(`/books/create`)
      .then(response => response.data)
  },
  update (title) {
    return $axios.put(`/books/${title}`)
      .then(response => response.data)
  },
  delete (title) {
    return $axios.delete(`/books/${title}`)
      .then(response => response.data)
  }
}
